package ru.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.testing.gateway.YandexTranslateGateway;

public class YandexTranslateTest {

    @Test
    public void getTranslate() {
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        String response = yandexTranslateGateway.getTranslate("Hello World!");
        Assertions.assertNotNull(response, "HTTP Error");
        Assertions.assertEquals("Всем Привет!",response, "Translate error");
    }

}