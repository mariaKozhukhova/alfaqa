package ru.testing.entities;

import com.google.gson.annotations.SerializedName;

public class Translations {
    @SerializedName("text")
    private String text;
    @SerializedName("detectedLanguageCode")
    private String detectedLanguageCode;

    public String getText() {
        return text;
    }

    public String getDetectedLanguageCode() {
        return detectedLanguageCode;
    }
}
