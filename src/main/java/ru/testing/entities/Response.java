package ru.testing.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {
    @SerializedName("translations")
    private List<Translations> translations;

    public List<Translations> getTranslations() {
        return translations;
    }
}
