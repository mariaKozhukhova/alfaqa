package ru.testing.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Request {
    @SerializedName("sourceLanguageCode")
    public String sourceLanguageCode;

    @SerializedName("targetLanguageCode")
    public String targetLanguageCode;

    @SerializedName("format")
    public String format;

    @SerializedName("texts")
    public List<String> texts;


    public void setSourceLanguageCode(String sourceLanguageCode) {
        this.sourceLanguageCode = sourceLanguageCode;
    }

    public void setTargetLanguageCode(String targetLanguageCode) {
        this.targetLanguageCode = targetLanguageCode;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setTexts(List<String> texts) {
        this.texts = texts;
    }

}
