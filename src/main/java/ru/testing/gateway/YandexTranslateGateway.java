package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.slf4j.Slf4j;
import ru.testing.entities.Request;
import ru.testing.entities.Response;
import ru.testing.entities.Translations;

import java.util.Collections;
import java.util.List;

@Slf4j
public class YandexTranslateGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String IAMTOKEN = "t1.9euelZqejY_KmceJmM7Hjp2MlonNku3rnpWam52PlJSNmM6QxsfPyZqQnJrl8_cCVgt2-e9FLGx0_d3z90IECXb570UsbHT9.fHmqVFZ6fFqv87bNRRirnCsNrZWoiSkXCFpfKRUE5SBb9TWyTtUedmFhFgcSubyjlETrwc2naBsvIg4wN2TICw";


    public String getTranslate(String text) {
        Request request = new Request();
        request.setSourceLanguageCode("en");
        List<String> texts = Collections.singletonList(text);
        request.setTexts(texts);
        request.setTargetLanguageCode("ru");
        request.setFormat("PLAIN_TEXT");

        Gson gson = new Gson();
        String json = gson.toJson(request);

        HttpResponse<String> httpResponse = null;
        try {
            httpResponse = Unirest.post(URL)
                    .header("Accept", "*/*")
                    .header("Authorization", "Bearer " + IAMTOKEN)
                    .body(json).asString();
        } catch (UnirestException e) {
            return null;
        }

        int status = httpResponse.getStatus();
        if (status != 200) {
            return null;
        }

        String body = httpResponse.getBody();
        Response response = gson.fromJson(body, Response.class);
        List<Translations> translations = response.getTranslations();
        Translations translations1 = translations.get(0);

        return translations1.getText();

    }
}